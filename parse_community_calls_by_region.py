def parse_community_calls_by_region_single_stratum(file, chromosome, start_coord, end_coord, stratum):
        communities = []
        input = open(file, 'r')
        for line in input:

                if line.split('\t')[0] == chromosome:
                        size = int(line.split('\t')[2]) - int(line.split('\t')[1])
                        if int(line.split('\t')[1]) < start_coord and int(line.split('\t')[2]) > start_coord:
                                if size > stratum[0] and size <= stratum[1]:
                                          communities.append((start_coord, float(line.split('\t')[2])))
                        elif int(line.split('\t')[2]) > end_coord and int(line.split('\t')[1]) < end_coord:
                                if size > stratum[0] and size <= stratum[1]:
                                          communities.append((float(line.split('\t')[1]), end_coord))
                        elif int(line.split('\t')[1]) > start_coord and int(line.split('\t')[2]) < end_coord:
                                if size > stratum[0] and size <= stratum[1]:
                                          communities.append((float(line.split('\t')[1]), float(line.split('\t')[2])))

        input.close()

        return communities





def parse_community_calls_by_region(file, chromosome, start_coord, end_coord):
	"""
	This script is meant to read in a genome-wide community file (like Ren TAD calls) and only extract communities within a region of interst. Community file should be of the form:
	Chr\tStart\tEnd
	"""

        communities = []
        input = open(file, 'r')
        for line in input:

                if line.split('\t')[0] == chromosome:
                        if int(line.split('\t')[1]) < start_coord and int(line.split('\t')[2]) > start_coord:
                                communities.append((start_coord, float(line.split('\t')[2])))
                        elif int(line.split('\t')[2]) > end_coord and int(line.split('\t')[1]) < end_coord:
                                communities.append((float(line.split('\t')[1]), end_coord))
                        elif int(line.split('\t')[1]) > start_coord and int(line.split('\t')[2]) < end_coord:
                                communities.append((float(line.split('\t')[1]), float(line.split('\t')[2])))

        input.close()

        return communities


def parse_netmod_community_calls_by_region(file, chromosome, start_coord, end_coord):
	"""
	This script reads in NetMod community calls and extracts communities within a region of interest (that may not necessarily be in the original region). This is to handle edge stiching of overlapping regions.
	Community file should be in the form: '#Region\tGamma\tCommunity_Assignment\tChr\tStart_Pos\tStop_Pos\tLeft_Variance\tRight_Variance'
	"""
	
	communities = []
	input = open(file, 'r')
	print 'start_coord: ', start_coord
	print 'end_cord: ', end_coord
	for line in input:
		if line.split('\t')[3] == chromosome:
			if int(line.split('\t')[4]) <= start_coord and int(line.split('\t')[5]) >= start_coord:
				communities.append((start_coord, float(line.split('\t')[5])))
			elif int(line.split('\t')[5]) >= end_coord and int(line.split('\t')[4]) <= end_coord:
				communities.append((float(line.split('\t')[4]), end_coord))
			elif int(line.split('\t')[4]) >= start_coord and int(line.split('\t')[5]) <= end_coord:
				communities.append((float(line.split('\t')[4]), float(line.split('\t')[5])))
			else:
				print 'community not in window'

	input.close()

	return communities


def parse_netmod_community_calls_single_region(file, chromosome, region):
	communities = []
	input = open(file, 'r')
	for line in input:
		if line.split('\t')[3] == chromosome:
			if line.split('\t')[0]==region:
				communities.append((float(line.split('\t')[4]), float(line.split('\t')[5])))
	input.close()
	return communities


def parse_netmod_community_calls_by_region_merged(file, chromosome, start_coord, end_coord):
        """
        This script reads in NetMod community calls and extracts communities within a region of interest (that may not necessarily be in the original region). This is to handle edge stiching of overlapping regions.
        Community file should be in the form: #Chr    Upstream_boundary_start Upstream_boundary_stop  Downstream_boundary_start       Downstream_boundary_stop
        """

        communities = []
	print 'looking for merged file: ', file
        input = open(file, 'r')
        print 'start_coord: ', start_coord
        print 'end_cord: ', end_coord
        for line in input:
                if line.split('\t')[0] == chromosome:
                        if int(line.split('\t')[1]) < start_coord and int(line.split('\t')[4]) > start_coord:
                                communities.append((start_coord, float(line.split('\t')[4])))
                        elif int(line.split('\t')[4]) > end_coord and int(line.split('\t')[1]) < end_coord:
                                communities.append((float(line.split('\t')[1]), end_coord))
                        elif int(line.split('\t')[1]) > start_coord and int(line.split('\t')[4]) < end_coord:
                                communities.append((float(line.split('\t')[1]), float(line.split('\t')[4])))
                        else:
                                print 'community not in window'

        input.close()

        return communities



def parse_netmod_community_calls_by_region_abbreviated(file, chromosome, start_coord, end_coord):
        """
        This script reads in NetMod community calls and extracts communities within a region of interest (that may not necessarily be in the original region). This is to handle edge stiching of overlapping regions.
        Community file should be in the form: '#Region\tGamma\tCommunity_Assignment\tChr\tStart_Pos\tStop_Pos\tLeft_Variance\tRight_Variance'
        """

        communities = []
        input = open(file, 'r')
        for line in input:
                if line.split('\t')[0] == chromosome:
                        if int(line.split('\t')[1]) < start_coord and int(line.split('\t')[2]) > start_coord:
                                communities.append((start_coord, float(line.split('\t')[2])))
                        elif int(line.split('\t')[2]) > end_coord and int(line.split('\t')[1]) < end_coord:
                                communities.append((float(line.split('\t')[1]), end_coord))
                        elif int(line.split('\t')[1]) > start_coord and int(line.split('\t')[2]) < end_coord:
                                communities.append((float(line.split('\t')[1]), float(line.split('\t')[2])))
				print 'appended community'

			else:
				print 'community not in window'
        input.close()

        return communities

def parse_netmod_community_calls_with_classification_by_region_abbreviated(file, chromosome, start_coord, end_coord, pvalue):

	communities = []
	input = open(file, 'r')
	for line in input:
		if line.split('\t')[0] == chromosome:
			if float(line.split('\t')[3])< pvalue:
				type1 = 'dynamic'
			else:
				type1 = 'constitutive'
			if float(line.split('\t')[4]) < pvalue:
				type2 = 'dynamic'
			else:
				type2 = 'constitutive'


                        if int(line.split('\t')[1]) < start_coord and int(line.split('\t')[2]) > start_coord:
                                communities.append((start_coord, float(line.split('\t')[2]), type1, type2))
                        elif int(line.split('\t')[2]) > end_coord and int(line.split('\t')[1]) < end_coord:
                                communities.append((float(line.split('\t')[1]), end_coord, type1, type2))
                        elif int(line.split('\t')[1]) > start_coord and int(line.split('\t')[2]) < end_coord:
                                communities.append((float(line.split('\t')[1]), float(line.split('\t')[2]), type1, type2))

        input.close()

        return communities


