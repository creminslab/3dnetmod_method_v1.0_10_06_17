import glob
import numpy as np

def main():
	chrs = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13', 'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chrX', 'chrY']
	fname = 'input/Brd2KOC8qnormed_40kb.counts'
	new_counts = open(fname, 'w')
	bin_count = 1
	for chr in chrs:
		matrix = np.load('input/Brd2KO_C8_qnormed_' + chr + '.npy')
		for i in range(len(matrix)):
			for j in range(len(matrix)):
				if i >= j:
					bin1 = i + bin_count 
					bin2 = j + bin_count
					output = str(bin1) + '\t' + str(bin2) + '\t' + str(matrix[i][j])
					print >> new_counts, output
		bin_count = bin_count + len(matrix)


	new_counts.close()
				
	




if __name__ == '__main__':
	main()
