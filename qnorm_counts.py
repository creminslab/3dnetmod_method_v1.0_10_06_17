from convert_matrices_to_npz import convert_matrices_to_npz
from remove_low_ffljs_qnorm import remove_low_ffljs_qnorm

def qnorm_counts(settings):

	convert_matrices_to_npz(settings)
	remove_low_ffljs_qnorm(settings)
	



