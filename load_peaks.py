


def load_peaks(bedfile):
    """
    Loads the features from a .bed file into dicts and returns them.

    Parameters
    ----------
    bedfile : str
        String reference to location of .bed file to load features from.

    Returns
    -------
    dict of lists of dicts
        The keys are chromosome names. The values are lists of features for that
        chromosome. The features are represented as dicts with the following
        structure::

            {
                'chrom': str,
                'start': int,
                'end'  : int,
            }


    """

    # dict to store features
    features = {}

    # parse bedfile
    with open(bedfile, 'r') as handle:
        for line in handle:
            # skip comments and track line
            if line.startswith('#') or line.startswith('track'):
                continue

            # split line
            pieces = line.strip().split('\t')

            # parse chromosome
            chromosome = pieces[0]

            # add chromosome to dict if this is a new one
            if chromosome not in features:
                features[chromosome] = []

            # parse feature information
            start = int(pieces[1])
            end = int(pieces[2])
            feature_id = None
            value = None

            # intelligent column schema guessing
            if len(pieces) >= 4:
                    try:
                        value = float(pieces[3])
                    except ValueError:
                        feature_id = pieces[3]
            if len(pieces) >= 5:
                    try:
                        value = float(pieces[4])
                    except ValueError:
                        feature_id = pieces[4]

            # dict to represent this feature
            peak_dict = {'chrom': chromosome,
                         'start': start,
                         'end'  : end}

            features[chromosome].append(peak_dict)

    return features
