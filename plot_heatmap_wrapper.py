#from load_binned_counts import load_binned_counts
#from load_pixelmap import load_pixelmap
from plot_heatmap import plot_heatmap
from get_colormap import get_colormap
#from get_colormap_edit import get_colormap
import numpy as np
from load_binned_counts import load_binned_counts
import sys
import os
from flatten_counts_single_region_geometric import flatten_counts_single_region


def main():
	counts_fname, bed_fname = sys.argv[1:]
	counts = load_binned_counts('input/' + counts_fname)
	bed_filename = 'input/' + bed_fname
	
	dir = 'output/heatmaps_no_communities/'
	if not os.path.isdir(dir): os.makedirs(dir)
	for region in counts:
		colorscale=(0.0,np.percentile(flatten_counts_single_region(counts[region], discard_nan=True),98))
		print region
		heatmap_filename =  dir + counts_fname + '_' + region + '.png'
		plot_heatmap(counts[region], heatmap_filename, colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, primer_file = (bed_filename), gene_track = 'input/gene_tracks/empty_track.txt')



if __name__ == '__main__':
	main()
	
