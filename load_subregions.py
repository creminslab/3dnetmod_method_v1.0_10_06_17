

def load_subregions(results_path):
        input = open(results_path, 'r')
        communities = [] # this will be a list of dicts

        for line in input:
                if line.startswith('#'):
                        continue
                else:
                        community = {}
                        community['chr'] = line.strip().split('\t')[0]
                        community['start'] = int(line.strip().split('\t')[1])
                        community['end'] = int(line.strip().split('\t')[2])
                        communities.append(community)
        input.close()

        return communities

